<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Utility Data Input</title>
    <script src="js/jquery-1.11.1.js"></script>
    <link rel="stylesheet" type="text/css" href="default.css">
</head>
<body>

    <script>

    //If document ready then load drop down boxes
    $(document).ready(function(){
        loadDropDown('#ddlObject', "form.php?q=objectddl");
        loadDropDown('#ddlPeriod', "form.php?q=periodddl");
    });

    //Load Drop Down
    function loadDropDown(objId, source) {
        $.getJSON(source, function(data){
          $.each(data, function(ddlValue, ddlLabel) {
            $(objId).append(
                $('<option></option>').val(this.ddlValue).text(this.ddlLabel)
            );
          });
        });
    } // END loadDropDown

    //Load Utility Entries Table
    function loadTable() {
    $(document).ready(function(){
        clearTable('tblUtilView');
    
        var Object = $("#ddlObject").val();
        var Period = $("#ddlPeriod").val();
        var url = "form.php";
        var param = "q=entrytbl&a=" + Object + "&b=" + Period;
        
        $.getJSON(url, param, function (data) {
            var tr;
            $.each(data, function(colKey, shortName, entryValue) {                    
                tr = $('<tr/>');
                tr.append("<td>" + this.colKey + "&nbsp;</td>");
                tr.append("<td>" + this.shortName + "&nbsp;</td>");
                tr.append("<td><input id='txt" + this.colKey + "' type='text' value='" + this.entryValue + "' onChange='saveValue(\"" + Object + "\",\"" + this.colKey + "\",\"" + Period + "\")'>&nbsp;</td>");
                tr.append("<td><label id='lbl" + this.colKey + "'></label></td>")
                $('#tblUtilView').append(tr);
            }); // END each
        }); // END get json
    }); // END doc ready
    } // END loadTable
    
    
    function saveValue(objKey, colKey, period) {
    $(document).ready(function(){
        var sourceText = "#txt" + colKey;
        var destText = "#lbl" + colKey;
        var params = "a=" + objKey;
        params += "&b=" + colKey;
        params += "&c=" + period;
        params += "&d=" + $(sourceText).val();
        
        //alert(objKey + " AND " + colKey + " AND " + period + " AND " + sourceText);
        //alert(params);

        $.ajax({
            type:"POST",
            url:"form.php?q=setvalue",
            data:params,
            success: function (html) {
                $(destText).text(html);
                if (html == "Updated") {
                    $(destText).addClass("txtUpdatePass");
                }
                else {
                    $(destText).addClass("txtUpdateFail");
                }
                setTimeout(function(){  $(destText).text(""); }, 3000);
            },
            error: function () {
                $(destText).text("Call Failed");
                $(destText).addClass("txtUpdateFail");
               setTimeout(function(){  $(destText).text(""); }, 3000);
            }      
        });
    
    }); // END doc ready 
    } // END saveValue
    
    //Function to clear any HTML Table of its rows
    function clearTable(tableName) {
        var Parent = document.getElementById(tableName);
        while(Parent.hasChildNodes())
        {
            Parent.removeChild(Parent.firstChild);
        }
    } // END clearTable
    
    </script>

    <h1>Utility Data Input</h1>
    <div>
    <br>
    <select id="ddlObject" onchange="loadTable()"></select>&nbsp;&nbsp;&nbsp;&nbsp;
    <select id="ddlPeriod" onchange="loadTable()"></select>
    <br><br>
    <table id="tblUtilView"></table>
    <form id="back">
        <br><button type="submit" form="back" formaction="index.php">Back</button>
    </form>
    </div>

</body>
</html>