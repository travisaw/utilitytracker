<?php

if ($_GET['q'] == "objectddl") { getObjectDDL(); }
if ($_GET['q'] == "periodddl") { getTimePeriodDDL(); }
if ($_GET['q'] == "entrytbl")  { getEntryTable(); }
if ($_GET['q'] == "setvalue")  { setEntryValue(); }
if ($_GET['q'] == "datatable") { getJsonTable(); }

/**********************************************************************************************************************************/
function getObjectDDL() {

    $sql = "SELECT `objKey` AS `ddlValue`, `shortName` AS `ddlLabel` FROM objectDefinition;";
    $content = getJson($sql);
    echo $content;

} // END getObjectDDL

/**********************************************************************************************************************************/
function getTimePeriodDDL() {

    $sql = "SELECT `id` AS `ddlValue`, `shortName` AS `ddlLabel` FROM objectPeriod WHERE `readOnly` = 0;";
    $content = getJson($sql);
    echo $content;

} // END getTimePeriodDDL

/**********************************************************************************************************************************/
function getEntryTable() {

    if (!isset($_GET['a']) || !isset($_GET['b'])) { die(); }

    $objectKey = $_GET['a'];
    $period = $_GET['b'];

    $sql = "SELECT A.colKey, A.shortName, IFNULL(B.value, A.`default`) AS `entryValue` " .
           "FROM objectField A " .
           "LEFT JOIN objectData B " .
           "ON A.colKey = B.colKey " .
           "AND (B.periodId = " . $period . " OR B.periodId IS NULL) " .
           "WHERE A.`type` = 1 " .
           "AND A.objKey = '" . $objectKey . "' " .
           "ORDER BY A.`sort`;";

    $content = getJson($sql);
    echo $content;

} // END getEntryTable

/**********************************************************************************************************************************/
function setEntryValue() {

    if (!isset($_POST['a']) || !isset($_POST['b']) || !isset($_POST['c']) || !isset($_POST['d'])) { die("Missing Parameters"); }
    $objKey = $_POST['a'];
    $colKey = $_POST['b'];
    $periodId = $_POST['c'];
    $value = $_POST['d'];

    $validate = validInput($objKey, $colKey, $value);
   
    if (!$validate[0]) {
        $errorText = $validate[1];
        die($errorText);
    }

    include 'dbconn.php';
    
    $sql = "CALL `insertData`('$objKey', '$colKey', $periodId, '$value');";

    if (!$result = $con->query($sql)) {
        //Log Error
        //die ("CALL failed: (" . $con->errno . ") " . $con->error);
        echo "Update Failed";
    }
    else {
        echo "Updated";
    }
    
    $con->close();

} // END setEntryValue

/**********************************************************************************************************************************/
function validInput($objKey, $colKey, $testValue) {

    include 'dbconn.php';

    $sql = "SELECT format FROM objectField WHERE objKey = '$objKey' AND colKey = '$colKey';";

    if (!$result = $con->query($sql)) { 
        //Log Error
        die ("CALL failed: (" . $con->errno . ") " . $con->error);
    }
    
    $validate = array(0 => false, 1 => '');

    if ($result->num_rows > 0) {
        $row = $result->fetch_row();
        
        switch ($row[0]) {
            case 1:
                /* At the moment we don't care about "String" data, could be any format */
                $validate[0] = true;
                break;
            case 2:
                if (is_numeric($testValue)) {
                    $validate[0] = true;
                }
                else {
                    $validate[0] = false;
                    $validate[1] = "Invalid Characters";
                }
                break;
            case 3:
                if (is_numeric($testValue)) {
                    $validate[0] = true;
                }
                else {
                    $validate[0] = false;
                    $validate[1] = "Invalid Characters";
                }
                break;
            case 4:
                $validate[0] = true;
                break;
            default:
                $validate[0] = false;
                $validate[1] = "Unknown Datatype";
        
        } // END SWITCH
    } // END IF
    else {
        $validate[0] = false;
        $validate[1] = "Key not found";
    } // END Else

    return $validate;

}

/**********************************************************************************************************************************/
function getJsonTable() {
    include 'dbconn.php';
    
    $objKey = $_GET["a"];
    
    // INITALIZE HEADER ARRAY
    $jsonRow[0] = "Period";
    // GET HEADER DETAILS
    $sql = "SELECT `shortName`, `colKey`, `default`, `isCurrency` FROM `objectField` WHERE `objKey` = '$objKey' ORDER BY `sort`;";

    if (!$result = $con->query($sql)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }

    if ($result->num_rows > 0) {
        $headerCount = 1;
        //$indexCount = 0;
        while($row = $result->fetch_row()) {
            $jsonRow[$headerCount] = $row[0];
            $columnIndex[$headerCount] = $row[1];
            if ($row[3] == 1) { $defaultRow[$indexCount] = "$"; }
            $defaultRow[$headerCount] .= $row[2];
            $headerCount++;
            $indexCount++;
        } // END WHILE LOOP
    } // END IF NUM ROWS > 0
    
    $jsonTable[0] = $jsonRow;
    
    // GET ROW DATA
    $sql = "SELECT A.`colKey`, A.`periodId`, A.`value`, B.`shortName` " .
           "FROM `objectData` A " .
           "INNER JOIN `objectPeriod` B " .
           "ON A.`periodId` = B.`id` " .
           "ORDER BY A.`periodId`;";

    if (!$result = $con->query($sql)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }

    if ($result->num_rows > 0) {
        $periodId = 1;
        $rowCount = 1;
        while($row = $result->fetch_row()) {
            // CHECK IF PERIOD ID HAS CHANGED.  IF SO ADD ROW TO RESULTS AND START NEW ROW.
            if ($row[1] != $periodId) {
                $periodId = $row[1];
                $jsonTable[$rowCount] = $jsonRow;
                $jsonRow = $defaultRow;
                $jsonRow[0] = $row[3];
                $rowCount++;
            } // END IF PEROID CHANGED

            $indexValue = array_search($row[0], $columnIndex);
            $jsonRow[$indexValue] = $row[2];
        
        } // END WHILE LOOP
    } // END IF NUM ROWS > 0



    // ENCODE AND RETURN JSON
    $response = json_encode($jsonTable, JSON_FORCE_OBJECT);
    echo $response;
    //print_r($jsonTable);
    $con->close();
}

/**********************************************************************************************************************************/
function getJson($sql) {
    include 'dbconn.php';

    if (!$result = $con->query($sql)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }

    if ($result->num_rows > 0) {

        /* Column Details */
        $columns = $result->fetch_fields();
        $colCount = 0;

        foreach($columns as $column) {
            $colName[$colCount] = $column->name;
            $colCount++;
        }

        /* Rows */
        $rowCount = 0;
        while($row = $result->fetch_row()) {
            $colCountRow = 0;
            while($colCountRow < $colCount) {
                $colNameRow = $colName[$colCountRow];
                $jsonRow[$colNameRow] = $row[$colCountRow];
                $colCountRow++;
            }
            $rowCount++;
            $jsonTable[$rowCount] = $jsonRow;
        }

        $response = json_encode($jsonTable, JSON_FORCE_OBJECT);
        return $response;
    }

} // END getJson

/**********************************************************************************************************************************/
function writeOut ($output) {

    $file = fopen("debug.txt","w");
    echo fwrite($file, $output);
    fclose($file);

}

?>