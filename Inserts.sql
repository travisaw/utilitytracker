TRUNCATE TABLE objectDefinition;
INSERT INTO objectDefinition (`objKey`, `shortName`, `longName`)
VALUES ('ELEC', 'Electricity', 'Electricity'),
('GAS', 'Gas', 'Gas'),
('WATR', 'Water', 'Water');

TRUNCATE TABLE objectPeriod;
INSERT INTO objectPeriod (`shortName`, `longName`)
VALUES ('November 2013', '11/01/2013 - 11/30/2013'),
('December 2013', '12/01/2013 - 12/31/2013'),
('January 2014', '01/01/2014 - 01/31/2014'),
('February 2014', '02/01/2014 - 02/28/2014');

TRUNCATE TABLE objectField;
INSERT INTO objectField (`objKey`, `colKey`, `sort`, `shortName`, `longName`, `type`, `formula`, `priority`, `format`, `default`, `isCurrency`)
VALUES ('ELEC', 'STDY', 1, 'Start Day', 'Billing Start Day', 1, NULL, 0, 4, '1/1/2014', 0),
('ELEC', 'ENDY', 2, 'End Day', 'Billing End Day', 1, NULL, 0, 4, '1/31/2014', 0),
('ELEC', 'STKW', 3, 'Start kWh', 'Start kWh', 1, NULL, 0, 2, 0, 0),
('ELEC', 'ENKW', 4, 'End kWh', 'End kWh', 1, NULL, 0, 2, 0, 0),
('ELEC', 'UKW', 5, 'Usage kWh', 'Total kWh Hours Used', 2, '<ENKW> - <STKW>', 1, 2, 0, 0),
('ELEC', 'PPKW', 6, 'Provider Price/kWh', 'Provider Price per kWh', 1, NULL, 0, 2, 0, 1),
('ELEC', 'PP', 7, 'Provider Price', 'Provider Price', 2, '<UKW> * <PPKW>', 2, 2, 0, 1),
('ELEC', 'UPKW', 8, 'Utility Price/kWh', 'Utility Price per kWh', 2, '<UP> / <UKW>', 2, 2, 0, 1),
('ELEC', 'UP', 9, 'Utility Price', 'Utility Price', 1, NULL, 0, 2, 0, 1),
('ELEC', 'USP', 10, 'Usage Charge', 'Usage Charge', 1, NULL, 0, 2, 0, 1),
('ELEC', 'TOTC', 11, 'Total Cost', 'Total Cost', 2, '<PP> + <UP> + <USP>', 3, 2, 0, 1),
('ELEC', 'REKW', 12, 'Real Price/kWh', 'Real Price kWH', 2, '<TOTC> / <UKW>', 4, 2, 0, 1),
('ELEC', 'NDAY', 13, '# Days', 'Number of Days in Period', 2, '<ENDY> - <STDY>', 1, 2, 0, 0),
('ELEC', 'PDAY', 14, 'Avg Price/Day', 'Average Price per Day', 2, '<PP> / <NDAY>', 2, 2, 0, 1),
('ELEC', 'TDAY', 15, 'Total Price/Day', 'Total Price per Day', 2, '<TOTC> / <NDAY>', 2, 2, 0, 1),
('ELEC', 'KDAY', 16, 'kWh/Day', 'Average Kilowatt hour per Day', 2, '<UKW> / <NDAY>', 2, 2, 0, 0),
('ELEC', 'HTMP', 17, 'Avg High Temp', 'Average High Temperature During Billing Period', 1, NULL, 0, 2, 0, 0),
('WATR', 'STDY', 1, 'Start Day', 'Billing Start Day', 1, NULL, 0, 4, '1/1/2014', 0),
('WATR', 'ENDY', 2, 'End Day', 'Billing End Day', 1, NULL, 0, 4, '1/31/2014', 0),
('WATR', 'STKG', 3, 'Start kGal', 'Start 1000 Gallons', 1, NULL, 0, 2, 0, 0),
('WATR', 'ENKG', 4, 'End kGal', 'End 1000 Gallons', 1, NULL, 0, 2, 0, 0),
('WATR', 'USKG', 5, 'Usage', 'Usage', 2, '<ENKG> - <STKG>', 1, 2, 0, 0),
('WATR', 'FEE1', 6, 'Water Fee', 'Water Fee', 1, NULL, 1, 2, 0, 1),
('WATR', 'FEE2', 7, 'Sewer Fee', 'Sewer Fee', 1, NULL, 1, 2, 0, 1),
('WATR', 'FEE3', 8, 'TCEQ Fee', 'TCEQ Fee', 1, NULL, 1, 2, 0, 1),
('WATR', 'TOTF', 9, 'Total', 'Total', 2, '<FEE1> + <FEE2> + <FEE3>', 1, 2, 0, 1),
('WATR', 'NDAY', 10, '# Days', 'Number of Days in Period', 2, '<ENDY> - <STDY>', 1, 2, 0, 0),
('WATR', 'PDAY', 11, 'Price/Day', 'Price per Day', 2, '<TOTF> / <NDAY>', 2, 2, 0, 1),
('WATR', 'UDAY', 11, 'Usage/Day', 'Usage per Day', 2, '<USKG> / <NDAY>', 2, 2, 0, 1);

TRUNCATE TABLE `objectData`;
INSERT INTO `objectData` (`id`, `objKey`, `colKey`, `periodId`, `value`) VALUES
(1, 'ELEC', 'STDY', 1, '11/7/2013'),
(2, 'ELEC', 'ENDY', 1, '11/25/2013'),
(3, 'ELEC', 'STKW', 1, '47830'),
(4, 'ELEC', 'ENKW', 1, '47888'),
(5, 'ELEC', 'PPKW', 1, '0.06'),
(6, 'ELEC', 'UP', 1, '7.97'),
(7, 'ELEC', 'USP', 1, '0.00'),
(8, 'ELEC', 'HTMP', 1, '69'),
(9, 'ELEC', 'STDY', 2, '11/25/2013'),
(10, 'ELEC', 'ENDY', 2, '12/30/2013'),
(11, 'ELEC', 'STKW', 2, '47888'),
(12, 'ELEC', 'ENKW', 2, '48325'),
(13, 'ELEC', 'PPKW', 2, '0.06'),
(14, 'ELEC', 'UP', 2, '24.78'),
(15, 'ELEC', 'USP', 2, '9.95'),
(16, 'ELEC', 'HTMP', 2, '61'),
(17, 'ELEC', 'STDY', 3, '12/30/2013'),
(18, 'ELEC', 'ENDY', 3, '1/29/2014'),
(19, 'ELEC', 'STKW', 3, '48325'),
(20, 'ELEC', 'ENKW', 3, '48830'),
(21, 'ELEC', 'PPKW', 3, '0.06'),
(22, 'ELEC', 'UP', 3, '27.50'),
(23, 'ELEC', 'USP', 3, '9.95'),
(24, 'ELEC', 'HTMP', 3, '61'),
(25, 'ELEC', 'STDY', 4, '1/29/2014'),
(26, 'ELEC', 'ENDY', 4, '2/27/2014'),
(27, 'ELEC', 'STKW', 4, '48830'),
(28, 'ELEC', 'ENKW', 4, '49246'),
(29, 'ELEC', 'PPKW', 4, '0.06'),
(30, 'ELEC', 'UP', 4, '24.33'),
(31, 'ELEC', 'USP', 4, '9.95'),
(32, 'ELEC', 'HTMP', 4, '63');
