
/* aggregate Table */

CREATE TABLE `aggregate` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`shortName` VARCHAR(20) NULL DEFAULT NULL,
	`longName` VARCHAR(100) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

/* aggregateObject Table */

CREATE TABLE `aggregateObject` (
	`aggregateId` INT(11) NOT NULL,
	`objectId` INT(11) NOT NULL,
	PRIMARY KEY (`aggregateId`, `objectId`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

/* eventLog Table */

CREATE TABLE `eventLog` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`logDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`eventType` VARCHAR(50) NOT NULL,
	`eventClass` VARCHAR(50) NULL DEFAULT NULL,
	`eventMsg` VARCHAR(500) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB;

/* objectDefinition Table */

CREATE TABLE `objectDefinition` (
	`objKey` VARCHAR(4) NOT NULL,
	`shortName` VARCHAR(20) NULL DEFAULT '',
	`longName` VARCHAR(100) NULL DEFAULT '',
	PRIMARY KEY (`objKey`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

/* objectField Table */

CREATE TABLE `objectField` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`objKey` VARCHAR(4) NOT NULL,
	`colKey` VARCHAR(4) NOT NULL,
	`sort` INT(10) UNSIGNED NOT NULL,
	`shortName` VARCHAR(20) NULL DEFAULT '',
	`longName` VARCHAR(100) NULL DEFAULT '',
	`type` TINYINT(4) UNSIGNED NOT NULL,
	`formula` VARCHAR(500) NULL DEFAULT NULL,
    `priority` tinyint(3) UNSIGNED NOT NULL,
    `format` tinyint(3) UNSIGNED NOT NULL,
    `default` VARCHAR(50) NULL DEFAULT '0',
	`isCurrency` TINYINT(3) UNSIGNED NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `IX_objectField_1` (`objKey`, `colKey`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

/* objectPeriod Table */

CREATE TABLE `objectPeriod` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`shortName` VARCHAR(20) NULL DEFAULT '',
	`longName` VARCHAR(100) NULL DEFAULT '',
    `readOnly` TINYINT(3) UNSIGNED NULL DEFAULT '0',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

/* objectData Table */

CREATE TABLE `objectData` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`objKey` VARCHAR(4) NOT NULL,
	`colKey` VARCHAR(4) NOT NULL,
	`periodId` INT(10) UNSIGNED NOT NULL,
	`value` VARCHAR(100) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `IX_objectData_1` (`objKey`, `colKey`, `periodId`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

/* param Table */

CREATE TABLE `param` (
	`valueKey` VARCHAR(32) NOT NULL,
	`valueString` VARCHAR(512) NULL DEFAULT NULL,
	PRIMARY KEY (`valueKey`)
)
COMMENT='System Parameters'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

/* ---------------------------------------------------------------------------------------------------------- */
/* Insert Params */

INSERT INTO param (`valueKey`, `valueString`)
VALUES ('Currency.Symbol', '$'),
       ('errorLog.INFO', '0'),
       ('errorLog.ERROR', '1'),
       ('errorLog.WARNING', '1');

/* ---------------------------------------------------------------------------------------------------------- */
DELIMITER $$$
/* Procedure updateRecipeIngredient */
DROP PROCEDURE IF EXISTS `insertData` $$$
CREATE PROCEDURE `insertData`
   (IN `inObjKey` VARCHAR(4),
    IN `inColKey` VARCHAR(4),
	IN `inPeriodId` INT,
	IN `inValue` VARCHAR(100)
    )
    LANGUAGE SQL
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT 'Inserts/Updates datapoints in objectData'
ThisSp:BEGIN

DECLARE varKeyExists INT;
DECLARE varPeriodExists INT;
DECLARE varValExists INT;

SELECT COUNT(*) INTO varKeyExists FROM objectField WHERE `objKey` = inObjKey AND `colKey` = inColKey;
SELECT COUNT(*) INTO varPeriodExists FROM objectPeriod WHERE `id` = inPeriodId;
SELECT COUNT(*) INTO varValExists FROM objectData WHERE `objKey` = inObjKey AND `colKey` = inColKey AND `periodId` = inPeriodId;

/* Validate Key Pair is Valid */
IF (varKeyExists < 1) THEN
    LEAVE ThisSp;
END If;

/* Validate time period is valid */
IF (varPeriodExists < 1) THEN
    LEAVE ThisSp;
END If;

/* If value exists then update, otherwise insert */
IF (varValExists > 0) THEN
    UPDATE objectData
    SET `value` = inValue
    WHERE `objKey` = inObjKey
    AND `colKey` = inColKey
    AND `periodId` = inPeriodId;
ELSE
    INSERT INTO objectData (`objKey`, `colKey`, `periodId`, `value`)
    VALUES (inObjKey, inColKey, inPeriodId, inValue);
END IF;

END
$$$