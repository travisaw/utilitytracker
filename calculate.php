<?php

    include 'dbconn.php';
    include 'matheval.php';
    
    $sql = "SELECT `objKey`, `colKey`, `formula`, `format`, `default` FROM objectField WHERE `type` = 2 ORDER BY priority";
    //$sql = $sql . " LIMIT 1";
    //echo $sql . "<br>";

    if (!$result = $con->query($sql)) { 
        die ("CALL failed: (" . $con->errno . ") " . $con->error);
    }
    
    // GET DETAILS ON EACH CALCULATED FIELD.
   if ($result->num_rows > 0) {
        $fieldCount = 0;
        while($row = $result->fetch_row()) {
            $objKey[$fieldCount] = $row[0];
            $colKey[$fieldCount] = $row[1];
            $formula[$fieldCount] = $row[2];
            $format[$fieldCount] = $row[3];
            $default[$fieldCount] = $row[4];

            $fieldCount++;
        } //END WHILE
    }
    else {
        // LOG MESSAGE
    } // END IF

    $processCount = 0;
    while ($processCount < $fieldCount) {
        // GET THE NAME OF THE VARIABLES THAT ARE BETWEEN THE <> IN THE FORMULA
        $variables = getFormulaVariables($formula[$processCount]);
        
        // VALIDATE VARIABLES ARE OF SAME TYPE
        if (validateData($variables, $objKey[$processCount], $colKey[$processCount], $format[$processCount], $con)) {
            //echo "Passed";

            // GET THE TEXT OF THE CALCULATIONS
            $formulas = getFormulas($variables, $objKey[$processCount], $formula[$processCount], $con);

            // NOW CALCULATE AND SAVE IF WE GOT A POPULATED FORMULA BACK
            if (!empty($formulas)) {
                setValue($objKey[$processCount], $colKey[$processCount], $formulas, $con);
            }
        }
        else {
            // LOG ERROR
            //echo "Failed";
        }

        $processCount++;
    } // END processing calcualted fields

    $con->close();

/***********************************************************************************************/
function getFormulas($variables, $objKey, $formula, $con) {

    $variableCount = 1;
    
    // FOR EACH VARIABLE IN THE EQUATION WE ARE GOING TO GET ALL VALUES AVAILABLE
    // IF A GIVEN VALUE IS NOT FOUND THE FORMULA FOR THAT PERIOD WILL NOT BE EVALUATED.
    foreach ($variables as $variable) {

        $sql = "SELECT B.`id`, C.`value` " .
        "FROM objectField A " .
        "INNER JOIN objectPeriod B " .
            "ON 1 = 1 " .
        "LEFT JOIN objectData C " .
            "ON A.`objKey` = C.`objKey` " .
            "AND A.`colKey` = C.`colKey` " .
            "AND B.`id` = C.`periodId` " .
        "WHERE A.`objKey` = '$objKey' " .
        "AND A.`colKey` = '$variable' " .
        "ORDER BY B.`id`;";

        if (!$result = $con->query($sql)) { 
            //WRITE ERROR
            die ("CALL failed: (" . $con->errno . ") " . $con->error);
        }

        if ($result->num_rows > 0) {
            while($row = $result->fetch_row()) {
                $periodId = $row[0];
                $value = $row[1];

                // IF VALUE IS NOT SET RECORD NO VALUE IN AUDIT ARRAY
                if ($variableCount == 1) {
                    $formulas[$periodId] = $formula;  // ADD FORMULA TO FINAL ARRAY
                    if (isset($value)) {
                        $audit[$periodId] = 1;
                        $formulas[$periodId] = str_replace("<$variable>" , $value, $formulas[$periodId]);
                    }
                    else {
                        $audit[$periodId] = 0;
                    }// END IF
                }
                else {
                    if (!isset($value) && $audit[$periodId] == 1) {
                        $audit[$periodId] = 0; 
                    }
                    else {
                        $formulas[$periodId] = str_replace("<$variable>" , $value, $formulas[$periodId]);
                    }// END IF
                    
                } // END IF VARIABLE COUNT = 1
            } // END WHILE FETCH ROW
        }// END IF NUM ROWS > 0
        else {
            // IF THERE ARE NO ROWS THEN CAN'T CALCULATE ANYTHING
            // LOG WARNING?
            return null;
        } // END IF

    $variableCount++;

    } //END FOR EACH VARIABLES

    // NOW LOOP THROUGH THE AUDIT ARRAY
    // IF THE PERIOD HAS AN INVALID FORMULA THEN REMOVE FROM FINAL ARRAY
    $finalAuditCount = 1;
    foreach ($audit as $finalAudit) {
        if ($finalAudit == 0) { unset($formulas[$finalAuditCount]); }
        $finalAuditCount++;
    } // END FOR EACH
    
    return $formulas;
    
} // END FUNCTION getFormulas

/***********************************************************************************************/
function getFormulaVariables($formulaText) {
    
    $fieldCount = substr_count($formulaText , "<");
    $loopCount = 1;
    
    while ($loopCount <= $fieldCount) {
        
        $fields[$loopCount] = getStringBetween($formulaText, "<", ">");
        $formulaText = substr($formulaText, strpos($formulaText, ">") + 1);
        $loopCount++;
    }
    
    return $fields;

} // END getFormulaVariables

/***********************************************************************************************/
function validateData($variables, $objKey, $colKey, $format, $con) {

    foreach ($variables as $variable) {

        $sql = "SELECT `format` FROM objectField WHERE `objKey` = '$objKey' AND `colKey` = '$variable'";

        if (!$result = $con->query($sql)) { 
            //WRITE ERROR
            die ("CALL failed: (" . $con->errno . ") " . $con->error);
        }
    
        if ($result->num_rows > 0) {
            $row = $result->fetch_row();
            if ($row[0] != $format) {
                //LOG INVALID FORMAT ON KEY
                //$error = "Datatype for $variable not the same as $colKey";
                return false;
            }
        }
        else {
            //LOG MISSING KEY
            //$error = "Key: $objKey Column: $variable doesn't exist!";
            return false;
        } // END IF

    } // END FOR EACH VARIABLE

    return true;

} // END validateData

function setValue($objKey, $colKey, $formulas, $con) {

    $expr = new Expression(); 

    foreach ($formulas as $valueKey => $value) {
        
        $mathResult = $expr->evaluate($value);

        $sql = "CALL `insertData`('" . $objKey . "', '" . $colKey . "', $valueKey, '$mathResult');";
        //echo "$sql<br>";

        if (!$result = $con->query($sql)) {
            //Log Error
            //die ("CALL failed: (" . $con->errno . ") " . $con->error);
            //echo "Update Failed";
        }
        else {
            //echo "Updated";
        }
    } // END For Each

} // END setValue

/***********************************************************************************************/
function getStringBetween($string, $start, $end){
    $string = " ".$string;
    $ini = strpos($string,$start);
    if ($ini == 0) return "";
    $ini += strlen($start);
    $len = strpos($string,$end,$ini) - $ini;
    return substr($string,$ini,$len);
} // END get_string_between

/***********************************************************************************************/
function logError() {

}

?>