<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Utility Data Input</title>
    <script src="js/jquery-1.11.1.js"></script>
    <link rel="stylesheet" type="text/css" href="default.css">
</head>
<body>

    <script>

    //If document ready then load drop down boxes
    $(document).ready(function(){
        loadDropDown('#ddlObject', "form.php?q=objectddl");
    });

    //Load Drop Down
    function loadDropDown(objId, source) {
        $.getJSON(source, function(data){
          $.each(data, function(ddlValue, ddlLabel) {
            $(objId).append(
                $('<option></option>').val(this.ddlValue).text(this.ddlLabel)
            );
          });
        });
    } // END loadDropDown


    //Load Utility Entries Table
    function loadTable() {
    $(document).ready(function(){
        clearTable('tblUtilView');

        var Object = $("#ddlObject").val();
        var url = "form.php";
        var param = "q=datatable&a=" + Object;

        $.getJSON(url, param, function (data) {
            $.each(data, function() {
                alert(data[0].length);
            
            }); // END each
        }); // END doc ready
    }); // END doc ready
    } // END loadTable


    //Function to clear any HTML Table of its rows
    function clearTable(tableName) {
        var Parent = document.getElementById(tableName);
        while(Parent.hasChildNodes())
        {
            Parent.removeChild(Parent.firstChild);
        }
    } // END clearTable

    </script>

    <h1>View Utility Data</h1>
    <div>
    <br>
    <select id="ddlObject" onchange="loadTable()"></select>
    <br><br>
    <table id="tblUtilView"></table>
    <form id="back">
        <br><button type="submit" form="back" formaction="index.php">Back</button>
    </form>
    </div>

</body>
</html>